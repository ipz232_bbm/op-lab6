#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <windows.h>
#include <Math.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	printf("������ ��������\n");
	int task;
	scanf("%d", &task);

	switch (task){

	case 1: // ������� �������� 1
		int month;
		printf("������ ���������� ����� �����\n");
		scanf("%d", &month);
		switch (month){
		case 1: printf("ѳ����"); break;
		case 2: printf("�����"); break;
		case 3: printf("��������"); break;
		case 4: printf("������"); break;
		case 5: printf("�������"); break;
		case 6: printf("�������"); break;
		case 7: printf("������"); break;
		case 8: printf("�������"); break;
		case 9: printf("��������"); break;
		case 10: printf("�������"); break;
		case 11: printf("��������"); break;
		case 12: printf("�������"); break;
		default:
			printf("����� �� ������ � ������� �� 1 �� 12");
			break;
		}
		break; // ��������� ��������

	case 2:	// ������� �������� 2
		int func;
		double x, y, funcf, res;
		printf("������ ��� �������:\nsin(x)[1]\nx^2[2]\ne^x[3]\n");
		scanf("%d", &func);

		if (func > 3 || func < 1) {
			printf("������� ���� ����� �� ������� ������� ������ ���� �������");
			break;
		}

		printf("x = ");
		scanf("%lf", &x);
		printf("y = ");
		scanf("%lf", &y);

		switch (func) {
		case 1:
			funcf = x * x;
			break;
		case 2:
			funcf = sin(x);
			break;
		case 3:
			funcf = exp(x);
			break;
		}

		if (fabs(x * y) > 10) {
			res = log(fabs(funcf) + fabs(y));
		}
		else if (fabs(x * y) == 10) {
			res = pow(fabs(funcf), 1. / 3) + y;
		}
		else {
			res = exp(func + y);
		}

		printf("res = %f", res);
		break;

	case 3: // ������� �������� 3
		int age, age_first;
		printf("������ ��\n");
		scanf("%d", &age);
		if (age < 20 || age > 69) {
			printf("������� �������� �� ������ � ������� 20-69");
			break;
		}

		age_first = age / 10;

		switch (age_first) {
		case 2: printf("�������� "); break;
		case 3: printf("�������� "); break;
		case 4: printf("����� "); break;
		case 5: printf("�'��������� "); break;
		case 6: printf("س��������� "); break;
		}

		age %= 10;

		switch (age) {
		case 1: printf("����  "); break;
		case 2: printf("��� "); break;
		case 3: printf("��� "); break;
		case 4: printf("������ "); break;
		case 5: printf("�'��� "); break;
		case 6: printf("����� "); break;
		case 7: printf("�� "); break;
		case 8: printf("��� "); break;
		case 9: printf("���'��� "); break;
		}

		if (age == 1) {
			printf("��");
			break;
		}
		else if (age > 1 && age < 5) {
			printf("����");
			break;
		}
		else {
			printf("����");
			break;
		}

	default:
		printf("������� ���� �������� �� ������� ������ ������� ��������");
		break;
	}
	return 0;
}